% ATL-PHYS-PROC-2020-047
% Matt LeBlanc <matt.leblanc@cern.ch>
% University of Arizona 2020

% Please make sure you insert your
% data according to the instructions in PoSauthmanual.pdf
\documentclass[a4paper,11pt]{article}
\usepackage{pos}
\usepackage{subfigure}

\title{Measurements of event shapes and jet substructure with ATLAS Run 2 data}
\ShortTitle{Event shapes and jet substructure with ATLAS}

\author*[a]{M. LeBlanc}
\author{on behalf of the ATLAS collaboration}

\affiliation[a]{University of Arizona,\\
  1118 E. Fourth Street, Tucson, USA}

\emailAdd{matt.leblanc@cern.ch}

\abstract{In order to achieve the highest levels of precision at the Large Hadron Collider, a detailed understanding of the strong interaction is required. Three recent measurements made by the ATLAS collaboration in $\sqrt{s}=13$~TeV $pp$ collisions are reported, which are sensitive to different aspects of perturbative and non-perturbative quantum chromodynamics. These results include a measurement of hadronic event shapes in multijet final states with large momentum transfer, a measurement of jet substructure quantities with jets groomed using the Soft Drop algorithm, and a measurement of the Lund jet plane using charged particle tracks inside of jets. These measurements are corrected for acceptance and detector effects, and are compared to state-of-the-art Monte Carlo models and analytical calculations.  The measured data have been made publicly available for use in future studies.}

\FullConference{%
  The Eighth Annual Conference on Large Hadron Collider Physics-LHCP2020 \\
  25-30 May, 2020\\
  online}

\begin{document}
\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}

\noindent In order to achieve the highest levels of precision at the Large Hadron Collider, a detailed understanding of the strong interaction is required. Three recent measurements made by the ATLAS collaboration~\cite{PERF-2007-01} in $\sqrt{s}=13$~TeV proton--proton collisions are reported, which are sensitive to different aspects of perturbative and non-perturbative quantum chromodynamics. These measurements are corrected for acceptance and detector effects, and are compared to state-of-the-art Monte Carlo (MC) models~\cite{ATL-PHYS-PUB-2019-017} and available analytical calculations.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Measurement of hadronic event shapes in multijet final states}

\noindent Event shapes are a family of observables that are used to characterise the flow of energy in collider events. Measurements of event shapes have been used to probe fundamental properties of QCD~\cite{BARTEL1980142,Kluth:2000km,OPAL:2011aa,Kluth:2009nx,Kluge:2006jm,Abbiendi:2008zz,Tanabashi:2018oca}, to tune MC models~\cite{Skands:2014pea} and to search for physics beyond the Standard Model (SM)~\cite{Anchordoqui:2010hi}. ATLAS has performed new measurements of several event shape observables~\cite{Aad:2020fch,EventShapesHepData}, revisiting this topic for the first time since early in Run 1.

The event shapes measured include the transverse thrust $T_{\perp}$ and its minor component $T_{m}$~\cite{BRANDT196457,PhysRevLett.39.1587}; the aplanarity $A$ and sphericity $S$, including the transverse sphericity projection $S_{\perp}$~\cite{PARISI197865,PhysRevD.20.2759}; and the $C$ and $D$ event shapes, which respectively vanish for two-jet and planar events. The large Run 2 dataset~\cite{DAPR-2018-01}, advances in jet reconstruction performance~\cite{Aad:2020flx} and MC models~\cite{ATL-PHYS-PUB-2019-017} allow these measurements to be binned in the jet multiplicity and the sum of the leading and subleading jet $p_{\text{T}}$. The measurements are normalised to the inclusive cross section of events with at least two jets.

The achieved precision tends to be limited at low jet multiplicity by differences in predictions from the various MC models studied in the context of the unfolding procedure (\emph{e.g.} Figure~\ref{fig:modelling}(a)), and at high jet multiplicity by the jet energy scale (\emph{e.g.} Figure~\ref{fig:modelling}(b)). The dominant component of the latter uncertainty is due to differences in the modelling of the gluon-jet response between {\sc{Pythia}}~8 and {\sc{Herwig++}}. The precision of the measurements depends on the fiducial region, but generally ranges between 1\%--10\%. Several MC models are compared to the measured data. The simulation describes the main features of the measurements, but agreement degrades in regions with large non-perturbative effects and in events with high jet multiplicity.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Measurement of soft-drop jet observables}

\noindent Jet substructure (JSS) presents new opportunities to learn about the strong interaction at high energy scales. Of particular interest in this context is the substructure of jets which have been groomed with the soft-drop (SD) algorithm~\cite{Dasgupta:2013ihk,Larkoski:2014wba}, which removes soft- and wide-angled radiation in a way that allows the groomed JSS distributions to be precisely described analytically. ATLAS has published a measurement of several JSS observables which characterise the hard splitting within a jet following SD grooming~\cite{Aad:2019vyi,1772062}, expanding on a prior measurement of the SD jet mass~\cite{STDM-2017-04}.

The SD algorithm proceeds by reclustering the constituents of a jet using the Cambridge-Aachen (C/A) jet clustering algorithm~\cite{Dokshitzer:1997in,Wobisch:1998wt}, then iterating through the angularly-ordered clustering history starting from the widest-angled splitting. At each step, the splitting between subjets $j_1$ and $j_2$, respectively the harder and softer branches of the clustering, is subjected to the SD condition:
\begin{equation}
\frac{\min \left( p_{\text{T}}^{j_{1}}, p_{\text{T}}^{j_{2}} \right)} {p_{\text{T}}^{j_{1}} + p_{\text{T}}^{j_{2}}} > z_{\text{cut}} \left( \frac{\Delta R(j_{1},j_{2})}{R} \right)^\beta\;,
\end{equation}
where $R$ is the ungroomed jet radius (here, $R=0.8$), and $z_{\text{cut}}$ and $\beta$ are parameters which control how much radiation is removed. If the splitting fails this condition, the softer subjet $j_2$ is discarded and grooming proceeds using $j_1$. If the inequality is satisfied, then the SD procedure terminates. In this measurement, dijet events with a leading jet $p_{\text{T}}>300$~GeV are selected. The SD parameter $z_{\text{cut}}$ is fixed at 0.1, and values of $\beta\in\{0,1,2\}$ are studied.

The properties of the hard splitting that stops the grooming procedure are studied, measuring the groomed jet mass $m$, the $p_{\text{T}}$ balance of the two primary subjets $z_{g} = j_2 / j_1$ and their opening angle, $r_g = \Delta R \left(j_1, j_2\right)$. These quantities are measured using calorimeter- and inner-detector based signals. The measured distributions are decomposed into quark- and gluon-like components using information from simulation. The inner-detector based measurements are more precise overall, but omit the neutral component of jet fragmentation. The calorimeter-based measurements contain information about all particles, and so these $m$ and $r_g$ measurements may be compared to several cutting-edge analytical predictions~\cite{Marzani:2017mva,Marzani:2017kqd,Frye:2016aiz,Frye:2016okc,Kang:2018vgn,Kang:2018jwa,Kang:2019prh}. Good agreement between the experimental data and these predictions is observed in regions where resummation effects are largest ($\Lambda_{\text{QCD}}/p_{\text{T}} \leq m/p_{\text{T}} < z_{\text{cut}}$). The accuracy of the descriptions deteriorates particularly as non-perturbative effects become more relevant. The overall precision is generally limited due to differences in the results obtained by unfolding with different MC models (Figure~\ref{fig:modelling}(c)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Measurement of the Lund jet plane using charged particles}

\noindent Rather than studying only the hardest perturbative splitting within a jet's formation, a recent phenomenological proposal~\cite{Dreyer:2018nbf} suggests an approach based on the framework of Lund diagrams~\cite{Andersson:1988gp} which can probe the entire jet clustering history. ATLAS has published a measurement of the Lund jet plane (LJP)~\cite{Aad:2020zcn,1790256}, using an inclusive sample of high-$p_{\text{T}}$ jets in 139~fb$^{-1}$ of Run 2 $pp$ collision data. Lund diagrams for individual $R=0.4$ jets are constructed by reclustering a jet's charged constituents with the C/A algorithm. The resultant angularly-ordered clustering sequence is then iteratively declustered; at each step, the relative momentum fraction ($z$) and angle ($\Delta R$) of the softer branch are taken as proxies for the kinematics of emissions from the hard-scatter parton. Dijet events with $R=0.4$ jet $p_{\text{T}}>675$~GeV are selected in this measurement.

The LJP is the two-dimensional space spanned by $\ln(1/z)~\emph{vs.}~\ln(R/\Delta R)$. Different physical effects factorise naturally in this space. This factorisation allows a single measurement to probe JSS in a way which is simultaneously sensitive to \emph{e.g.} different parton shower and hadronisation models, but also isolates sensitivity to each process within expected regions of the LJP distribution. The precision of this measurement is typically $\sim\mathcal{O}(10\%)$, limited by differences in the final result observed when unfolding the data using different MC models (Figure~\ref{fig:modelling}(d)). The measured data tend to disagree significantly with the predictions of several state-of-the-art MC models, indicating that this measurement could be a useful resource for future non-perturbative model-building and parton shower MC tuning efforts. Recently, an analytical prediction of the primary LJP density has been published~\cite{Lifson:2020gua}, which agrees well with this measurement in regions of the LJP where non-perturbative effects are small.

\begin{figure}[ht]
\begin{center}
\subfigure[]{\includegraphics[width=0.41\textwidth]{figures/fig_02c.pdf}}
\subfigure[]{\includegraphics[width=0.41\textwidth]{figures/fig_02d.pdf}}\\
\subfigure[]{\includegraphics[width=0.49\textwidth]{figures/fig_05a.pdf}}
\subfigure[]{\includegraphics[width=0.39\textwidth]{figures/fig_03b.pdf}}
\caption{Selected figures from (a,b)~Reference~\cite{Aad:2020fch}, (c)~Reference~\cite{Aad:2019vyi} and~(d)~Reference~\cite{Aad:2020zcn}. In each case, the precision of these analyses is limited by differences between MC models. In cases where the experimental uncertainty due to the jet energy scale is large, the most relevant subcomponent of the uncertainty in these measurements is related to differences in the modelling of gluon jets between various standard MC generators.}\label{fig:modelling}
\end{center}
\end{figure}

\section{Concluding remarks}

\noindent ATLAS has a vibrant jet physics programme studying the dynamics of the strong interaction using both traditional differential cross-section measurements and cutting-edge JSS techniques. In each measurement presented here, and as highlighted in Figure~\ref{fig:modelling}, the precision is limited in at least some portion of the fiducial space by disagreements between MC models. The measured data have been made publicly available for use in future studies~\cite{1790256,1772062}, in order to help facilitate the maximum possible precision of future physics analyses performed at the LHC.

\clearpage
\bibliographystyle{JHEP}
\bibliography{ATLAS,LHCP,PubNotes}

\end{document}
